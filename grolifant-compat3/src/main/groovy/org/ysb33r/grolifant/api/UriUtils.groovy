/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api

import groovy.transform.CompileStatic

/** Dealing with URIs
 *
 * @deprecated Use methods from {@link org.ysb33r.grolifant.api.v4.UriUtils}
 */
@CompileStatic
class UriUtils {

    /** Attempts to convert object to a URI.
     *
     * Closures can be passed and will be evaluated. Result will then be converted to a URI.
     *
     * @param uriThingy Anything that could be converted to a URI
     * @return URI object
     *
     * @deprecated
     */
    static URI urize(final Object uriThingy) {
        org.ysb33r.grolifant.api.v4.UriUtils.urize(uriThingy)
    }

    /** Get final package or directory name from a URI
     *
     * @param uri
     * @return Last part of URI path.
     * @deprecated
     *
     * @since 0.5
     */
    @SuppressWarnings('UnnecessarySubstring')
    static String getPkgName(final URI uri) {
        org.ysb33r.grolifant.api.v4.UriUtils.getPkgName(uri)
    }

    /** Creates a SHA-256 has of a URI.
     *
     * @param uri URI to hash
     * @return
     * @deprecated
     *
     * @since 0.5
     */
    static String hashURI(final URI uri) {
        org.ysb33r.grolifant.api.v4.UriUtils.hashURI(uri)
    }

    /** Create a URI where the user/password is masked out.
     *
     * @param uri Original URI
     * @return URI with no credentials.
     * @deprecated
     *
     * @since 0.8
     */
    static URI safeUri(URI uri) {
        org.ysb33r.grolifant.api.v4.UriUtils.safeUri(uri)
    }
}
