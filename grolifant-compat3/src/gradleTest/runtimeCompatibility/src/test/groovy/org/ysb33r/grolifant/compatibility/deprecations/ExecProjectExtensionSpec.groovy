/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.deprecations


import org.gradle.api.Project
import org.gradle.process.ExecResult
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.ExtensionUtils
import org.ysb33r.grolifant.api.core.OperatingSystem
import org.ysb33r.grolifant.api.exec.ExecSpecInstantiator
import org.ysb33r.grolifant.compatibility.deprecations.git.SimpleGitExecSpec
import spock.lang.Specification

import java.nio.file.Path
import java.nio.file.attribute.PosixFilePermission

import static java.nio.file.Files.getPosixFilePermissions
import static java.nio.file.Files.setPosixFilePermissions
import static java.nio.file.attribute.PosixFilePermission.OWNER_EXECUTE

@SuppressWarnings(['UnnecessarySetter', 'LineLength'])
class ExecProjectExtensionSpec extends Specification {

    public static final OperatingSystem OS = OperatingSystem.current()
    public static final File TESTDIST_DIR = new File(System.getProperty('COMPAT_TEST_RESOURCES_DIR') ?: 'src/gradleTest/runtimeCompatibility/src/test/resources').absoluteFile
    public static final String toolExt = OS.windows ? 'cmd' : 'sh'


    Project project = ProjectBuilder.builder().build()

    void 'Add execution specification to project as extension'() {
        when:
        // tag::adding-extension[]
        ExtensionUtils.addExecProjectExtension('gitexec', project, { Project project ->
            new SimpleGitExecSpec(project)
        } as ExecSpecInstantiator<SimpleGitExecSpec>) // <1>
        // end::adding-extension[]

        then:
        project.extensions.extraProperties.get('gitexec')
    }

    void 'Run executable as extension'() {
        setup:
        File targetExecutable = new File(TESTDIST_DIR, "mycmd.${toolExt}")
        ExtensionUtils.addExecProjectExtension('myrunner', project, { Project project ->
            new SimpleGitExecSpec(project)
        } as ExecSpecInstantiator<SimpleGitExecSpec>)

        OutputStream output = new ByteArrayOutputStream()
        setExecutePermission(targetExecutable)

        when:
        Closure configurator = {
            command 'install'
            standardOutput output
            executable targetExecutable
        }

        ExecResult result = project.myrunner configurator

        then:
        result.exitValue == 0
        output.toString().startsWith('install')
    }

    void setExecutePermission(File target) {
        if (!OS.windows) {
            Path path = target.toPath()
            Set<PosixFilePermission> perms = getPosixFilePermissions(path)
            perms.add(OWNER_EXECUTE)
            setPosixFilePermissions(path, perms)
        }
    }
}