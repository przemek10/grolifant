/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v6

import org.gradle.api.Project
import org.gradle.api.file.CopySpec
import org.gradle.api.provider.Provider
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.loadable.v5.DefaultProjectOperations
import spock.lang.Specification

class DefaultProjectOperationsSpec extends Specification {

    Project project = ProjectBuilder.builder().build()
    DefaultProjectOperations grolifant

    void setup() {
        grolifant = new DefaultProjectOperations(project)
    }


    void 'Can fileize a Provider'() {
        given:
        String subdir = 'src'
        Provider<String> theFile = project.provider({ -> subdir })

        when:
        File result = grolifant.file(theFile)

        then:
        result.absolutePath == project.file(subdir).absolutePath
    }

    void 'Can get a project cache directory'() {
        expect:
        grolifant.projectCacheDir != null
    }

    void 'Update a property (Gradle 5.0+)'() {
        setup:
        def oldProv = project.objects.property(File)
        oldProv.set(new File('foo'))
        def cachedProv = oldProv

        when:
        grolifant.updateFileProperty(oldProv, 'bar')

        then:
        oldProv.get() == project.file('bar')
        cachedProv.get() == project.file('bar')
    }

    void 'Copy a file'() {
        setup:
        new File(project.projectDir, 'foo.txt').text = 'text'
        def dest = grolifant.buildDir
        def action = { CopySpec cs ->
            cs.from 'foo.txt'
            cs.into dest
        }

        when:
        grolifant.copy(action)

        then:
        new File(grolifant.buildDir.get(), 'foo.txt').exists()
    }
}