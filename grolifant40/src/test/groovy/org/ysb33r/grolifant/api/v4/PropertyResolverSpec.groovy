/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.v4.PropertyResolver
import spock.lang.Specification
import spock.lang.Unroll

import static org.ysb33r.grolifant.internal.v4.property.order.PropertyNaming.asEnvVar

class PropertyResolverSpec extends Specification {

    Project project = ProjectBuilder.builder().build()

    // tag::create-property-resolver[]
    PropertyResolver resolver = new PropertyResolver(project)
    // end::create-property-resolver[]

    void 'Can read a system property'() {
        expect:
        resolver.get('property.provider.test.system') == 'system-property'
    }

    void 'Can read an environment variable'() {
        expect:
        resolver.get('property.provider.test.env') == 'environment-var'
    }

    void 'Can return null is nothing is defined()'() {
        expect:
        resolver.get('foo.bar.king.kong') == null
    }

    void 'Can return default value if nothing is defined'() {
        expect:
        resolver.get('foo.bar.king.kong', '123') == '123'
    }

    void 'Can use alternative order'() {
        expect:
        resolver.get('foo.bar.king.kong', resolver.SYSTEM_ENV_PROPERTY) == null
        resolver.get('foo.bar.king.kong', '123', resolver.SYSTEM_ENV_PROPERTY) == '123'
    }

    @Unroll
    void '#value is convertible to environmental variables'() {
        expect:
        asEnvVar(value) == 'A_B_C'

        where:
        value << ['a.b.c', 'a-b-c']
    }
}
