/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v4

import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.errors.UnsupportedConfigurationException
import org.ysb33r.grolifant.api.v4.TaskProvider
import spock.lang.Specification

import java.util.concurrent.Callable

import static org.ysb33r.grolifant.api.v4.TaskUtils.taskize

class TaskUtilsSpec extends Specification {
    public static final String TASK_NAME = 'foo'

    Project project = ProjectBuilder.builder().build()
    Task task

    void setup() {
        task = project.tasks.create(TASK_NAME)
    }

    void 'Return the same task when passed a task'() {
        expect:
        taskize(project, task) == task
    }

    void 'Return the correct task from a Gradle TaskProvider'() {
        setup:
        def provider = project.provider({ -> task } as Callable<Task>)

        expect:
        taskize(project, provider) == task
    }

    void 'Return the correct task from a Grolifant TaskProvider'() {
        setup:
        def provider = TaskProvider.taskByName(project, TASK_NAME)

        expect:
        taskize(project, provider) == task
    }

    void 'Return the correct task from a string'() {
        expect:
        taskize(project, TASK_NAME) == task
    }

    void 'Return the correct task from a closure containing a string'() {
        setup:
        def provider = { -> TASK_NAME }

        expect:
        taskize(project, provider) == task
    }

    void 'Throw an exception is a file object if passed'() {
        when:
        taskize(project, new File(TASK_NAME))

        then:
        thrown(UnsupportedConfigurationException)
    }

    void 'Throw an exception if the task does not exist'() {
        when:
        taskize(project, 'nonExisting')

        then:
        thrown(org.gradle.api.UnknownTaskException)
    }

    void 'Convert a list of items to tasks'() {
        setup:
        def tasks = [
            task,
            TASK_NAME,
            project.provider({ -> task } as Callable<Task>),
            [task, TASK_NAME],
            [foo: task],
            project.provider({ -> [TASK_NAME, TASK_NAME] } as Callable<List<String>>)
        ]

        when:
        List<Task> result = taskize(project, tasks)

        then:
        result.size() == 8
        result.count { it == task } == 8
    }
}