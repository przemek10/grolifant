/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.internal.v6

import org.gradle.api.Project
import org.gradle.api.file.FileSystemOperations
import org.gradle.api.internal.project.ProjectInternal
import org.ysb33r.grolifant.api.core.FileSystemOperationsProxy

/**
 * Wraps a real FileSystemsOperation service.
 *
 * Used on Gradle 6.0+
 *
 * @since 1.0.0
 */
class DefaultFileSystemOperations implements FileSystemOperationsProxy {

    DefaultFileSystemOperations(Project project) {
        fso = ((ProjectInternal) project).services.get(FileSystemOperations)
    }

    @Delegate
    private final FileSystemOperations fso
}
