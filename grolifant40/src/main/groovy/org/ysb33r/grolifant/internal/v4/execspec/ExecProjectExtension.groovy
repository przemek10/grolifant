/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.internal.v4.execspec

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.process.ExecResult
import org.gradle.process.ExecSpec
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.v4.exec.ExecSpecInstantiator
import org.ysb33r.grolifant.api.v4.exec.ToolExecSpecGroovy

/**
 *
 * @since 0.3
 */
@CompileStatic
class ExecProjectExtension<T extends ToolExecSpecGroovy> {

    @Deprecated
    ExecProjectExtension(final Project project, final ExecSpecInstantiator<T> instantiator) {
        this.projectOperations = ProjectOperations.create(project)
        this.instantiator = instantiator
    }

    /**
     *
     * @param projectOps
     * @param instantiator
     *
     * @since 1.0
     */
    ExecProjectExtension(final ProjectOperations projectOps, final ExecSpecInstantiator<T> instantiator) {
        this.projectOperations = projectOps
        this.instantiator = instantiator
    }

    ExecResult call(Action<T> execSpecConfigurator) {
        T execSpec = instantiator.create(projectOperations)
        execSpecConfigurator.execute(execSpec)
        execute(execSpec)
    }

    ExecResult call(Closure execSpecConfigurator) {
        T execSpec = instantiator.create(projectOperations)
        execSpec.configure execSpecConfigurator
        execute(execSpec)
    }

    ExecResult execute(T execSpec) {
        Closure runner = { T fromSpec, ExecSpec toSpec ->
            fromSpec.copyToExecSpec(toSpec)
        }
        projectOperations.exec(runner.curry(execSpec) as Action<ExecSpec>)
    }

    private final ProjectOperations projectOperations
    private final ExecSpecInstantiator<T> instantiator
}
