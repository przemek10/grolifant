/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.internal.v4

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.process.ExecResult
import org.gradle.process.ExecSpec
import org.gradle.process.JavaExecSpec
import org.ysb33r.grolifant.api.core.ExecOperationsProxy

@CompileStatic
class DefaultExecOperations implements ExecOperationsProxy {

    DefaultExecOperations(Project project) {
        this.project = project
    }

    @Override
    ExecResult exec(Action<? super ExecSpec> action) {
        this.project.exec(action)
    }

    @Override
    ExecResult javaexec(Action<? super JavaExecSpec> action) {
        this.project.javaexec(action)
    }

    private final Project project
}
