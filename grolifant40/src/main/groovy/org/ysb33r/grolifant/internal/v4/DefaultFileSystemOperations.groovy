/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.internal.v4

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.file.CopySpec
import org.gradle.api.file.DeleteSpec
import org.gradle.api.tasks.WorkResult
import org.ysb33r.grolifant.api.core.FileSystemOperationsProxy

/**
 * Legacy operations to deal with filesystem operations in a way that
 * will allow forward compatibility with Gradle 6.0+.
 *
 * @since 1.0.0
 */
@CompileStatic
class DefaultFileSystemOperations implements FileSystemOperationsProxy {

    DefaultFileSystemOperations(Project project) {
        this.project = project
    }

    @Override
    WorkResult copy(Action<? super CopySpec> action) {
        project.copy(action)
    }

    @Override
    WorkResult delete(Action<? super DeleteSpec> action) {
        project.delete(action)
    }

    private final Project project
}
