/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.internal.v4

import org.gradle.api.Transformer
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant.api.v4.PropertyStore

/**
 * Wraps an actual Gradle Property.
 *
 * Used with Gradle 4.3+.
 *
 * @param <T> Property type
 *
 * @since 1.0.0
 */
class ActualProperty<T> implements PropertyStore<T> {

    ActualProperty(ObjectFactory objects, Class<T> clazz) {
        prop = objects.property(clazz)
    }

    ActualProperty(ObjectFactory objects, T value) {
        prop = objects.property(clazz)
        prop.set(value)
    }

    @Override
    Object getAsProperty() {
        prop
    }

    @Override
    Provider<T> getAsProvider() {
        prop
    }

    @Override
    void set(T t) {
        prop.set(t)
    }

    @Override
    void set(Provider<? extends T> provider) {
        prop.set(provider)
    }

    @Override
    T get() {
        prop.get()
    }

    @SuppressWarnings('UnnecessaryGetter')
    @Override
    T getOrNull() {
        prop.getOrNull()
    }

    @Override
    T getOrElse(T t) {
        prop.getOrElse(t)
    }

    @Override
    def <S> Provider<S> map(Transformer<? extends S, ? super T> transformer) {
        prop.map(transformer)
    }

    @Override
    def <S> Provider<S> flatMap(Transformer<? extends Provider<? extends S>, ? super T> transformer) {
        prop.flatMap(transformer)
    }

    @Override
    Provider<T> orElse(T value) {
        prop.orElse(value)
    }

    @Override
    Provider<T> orElse(Provider<? extends T> provider) {
        prop.orElse(provider)
    }

    @Override
    boolean isPresent() {
        prop.present
    }

    private final Property<T> prop
}
