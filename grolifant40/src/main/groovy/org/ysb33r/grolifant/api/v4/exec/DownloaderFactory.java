/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.exec;

import org.gradle.api.Project;
import org.ysb33r.grolifant.api.core.ProjectOperations;
import org.ysb33r.grolifant.api.v4.AbstractDistributionInstaller;

import java.util.Map;

/**
 * A factory for creating tool/distribution downloaders.
 *
 * @param <T> An implementation of {@link AbstractDistributionInstaller}
 *
 * @since 1.0
 */
public interface DownloaderFactory<T extends AbstractDistributionInstaller> {
    /** Creates a downloader
     *
     * @param options An arbitrary map of options. This could interpreted by the factory or simply ignored.
     * @param version The version of the exe that is required.
     * @param projectOperations The associated Gradle project operations.
     * @return An instance of a downloader
     */
    T create(Map<String, Object> options, String version, ProjectOperations projectOperations);
}
