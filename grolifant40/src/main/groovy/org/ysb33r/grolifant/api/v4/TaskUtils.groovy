/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.tasks.TaskContainer
import org.ysb33r.grolifant.api.errors.UnsupportedConfigurationException

import java.util.concurrent.Callable

import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_4_3
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_4_5
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_4_8
import static org.ysb33r.grolifant.api.v4.StringUtils.stringize

/** Utilities for dealing with tasks.
 *
 * @author Schalk W. Cronje
 *
 * @since 0.17.0
 */
@CompileStatic
class TaskUtils {

    /** Resolves a list of items to a list of tasks.
     *
     * List items can be any of the types supported by {@link #taskize(Project project, Object o)}, but
     * in addition the following are supported:
     *
     * <ul>
     *     <li>Map</li>
     *     <li>Iterable / Collection</li>
     * </ul>
     *
     * Embedded lists are resolved recursively and the final result is a flattend list. This will be done even when
     * a {@code Provider}, {@code Callable} or {@code Closure} returns an iterable sequence.
     *
     * @param project Project context
     * @param taskyThings Iterable sequence of items to be resolved.
     * @return LIst of resolved tasks.
     * @throw {@link UnsupportedConfigurationException} if any of the objects cannot be converted to a task.
     * @throw {@link org.gradle.api.UnknownTaskException} if the conversion is supported but the task cannot be located.
     * @deprecated Use the version of this method that takes a {@link TaskContainer} instead as it will be
     *   safe when configuration cache is active.
     */
    @Deprecated
    static List<Task> taskize(final Project project, final Iterable<Object> taskyThings) {
        taskize(project.tasks, taskyThings)
    }

    /** Resolves a list of items to a list of tasks.
     *
     * List items can be any of the types supported by {@link #taskize(Project project, Object o)}, but
     * in addition the following are supported:
     *
     * <ul>
     *     <li>Map</li>
     *     <li>Iterable / Collection</li>
     * </ul>
     *
     * Embedded lists are resolved recursively and the final result is a flattend list. This will be done even when
     * a {@code Provider}, {@code Callable} or {@code Closure} returns an iterable sequence.
     *
     * @param tasks The current project's task container.
     * @param taskyThings Iterable sequence of items to be resolved.
     * @return LIst of resolved tasks.
     * @throw {@link UnsupportedConfigurationException} if any of the objects cannot be converted to a task.
     * @throw {@link org.gradle.api.UnknownTaskException} if the conversion is supported but the task cannot be located.
     *
     * @since 1.0
     */
    static List<Task> taskize(final TaskContainer tasks, final Iterable<Object> taskyThings) {
        List<Task> collection = []
        for (Object item in taskyThings) {
            if (isIterableProperty(item)) {
                resolveIterablePropertyTo(collection, tasks, item)
            } else {
                switch (item) {
                    case Map:
                        collection.addAll(taskize(tasks, (Iterable) ((Map) item).values()))
                        break
                    case Iterable:
                        collection.addAll(taskize(tasks, (Iterable) item))
                        break
                    case Provider:
                        resolveSingleItemOrIterableTo(collection, tasks, ((Provider) item).get())
                        break
                    case Callable:
                        resolveSingleItemOrIterableTo(collection, tasks, ((Callable) item).call())
                        break
                    default:
                        collection.add(taskize(tasks, item))
                }
            }
        }
        collection
    }

    /** Resolves a single item to an existing task instance.
     *
     * The following types are supported:
     * <ul>
     *     <li>Any Gradle {@link Task}</li>
     *     <li>Any Gradle {@link org.gradle.api.tasks.TaskProvider} since Gradle 4.8+</li>
     *     <li>Any Grolifant {@link org.ysb33r.grolifant.api.v4.TaskProvider}</li>
     *     <li>Any standard Java {@code CharSequence}</li>
     *     <li>Any {@link Provider} of the above types</li>
     *     <li>Any Groovy Closure that returns one of the above types.</li>
     *     <li>Any Java {@code Callable} that returns one of the above types</li>
     * </ul>
     *
     * Resolving occurs recursively if necessary.
     *
     * @param tasks Task container for current project.
     * @param t Object to resolve to task
     * @return Resolved task instance.
     * @throw {@link UnsupportedConfigurationException} if the object cannot be converted to a task.
     * @throw {@link org.gradle.api.UnknownTaskException} if the conversion is supported but the task cannot be located.
     *
     * @since 1.0
     */
    static Task taskize(final TaskContainer tasks, final Object t) {
        if (isGradleTaskProvider(t)) {
            return gradleTaskProviderToTask(t)
        }

        switch (t) {
            case Task:
                return (Task) t
            case TaskProvider:
                return ((TaskProvider) t).get()
            case Provider:
                return taskize(tasks, ((Provider) t).get())
            case Callable:
                return taskize(tasks, ((Callable) t).call())
            case Closure:
                return taskize(tasks, ((Closure) t).call())
            case CharSequence:
                return tasks.getByName(stringize(t))
            default:
                throw new UnsupportedConfigurationException("Cannot convert ${t.class.name} to a task")
        }
    }

    /** Resolves a single item to an existing task instance.
     *
     * The following types are supported:
     * <ul>
     *     <li>Any Gradle {@link Task}</li>
     *     <li>Any Gradle {@link org.gradle.api.tasks.TaskProvider} since Gradle 4.8+</li>
     *     <li>Any Grolifant {@link org.ysb33r.grolifant.api.v4.TaskProvider}</li>
     *     <li>Any standard Java {@code CharSequence}</li>
     *     <li>Any {@link Provider} of the above types</li>
     *     <li>Any Groovy Closure that returns one of the above types.</li>
     *     <li>Any Java {@code Callable} that returns one of the above types</li>
     * </ul>
     *
     * Resolving occurs recursively if necessary.
     *
     * @param project Project context
     * @param t Object to resolve to task
     * @return Resolved task instance.
     * @throw {@link UnsupportedConfigurationException} if the object cannot be converted to a task.
     * @throw {@link org.gradle.api.UnknownTaskException} if the conversion is supported but the task cannot be located.
     * @deprecated Use the version of theis method that take a {@link TaskContainer} instead as that method will be
     *   safe when configuration cache is active.
     */
    @Deprecated
    static Task taskize(final Project project, final Object t) {
        taskize(project.tasks, t)
    }

    /** Creates a basic task {@link Provider} instance from an object.
     *
     * @param tasks Task container for this project.
     * @param o Object to be evaluated to a task.
     * @return Lazy-evaluatable task.
     *
     * @since 1.0
     */
    static Provider<Task> taskProviderFrom(TaskContainer tasks, ProviderFactory providerFactory, Object o) {
        providerFactory.provider({ ->
            taskize(tasks, o)
        } as Callable<Task>)
    }

    /** Creates a basic task {@link Provider} instance from an object.
     *
     * @param project Project context.
     * @param o Object to be evaluated to a task.
     * @return Lazy-evaluatable task.
     * @deprecated Use the version that takes a {@link TaskContainer} and {@link ProviderFactory} instead.
     */
    @Deprecated
    static Provider<Task> taskProviderFrom(Project project, Object o) {
        taskProviderFrom(project.tasks, project.providers, o)
    }

    @CompileDynamic
    static private boolean isGradleTaskProvider(Object o) {
        if (PRE_4_8) {
            false
        } else {
            o instanceof org.gradle.api.tasks.TaskProvider
        }
    }

    @CompileDynamic
    static private Task gradleTaskProviderToTask(Object taskProvider) {
        if (PRE_4_8) {
            throw new UnsupportedConfigurationException('org.gradle.api.tasks.TaskProvider is not supported')
        } else {
            ((org.gradle.api.tasks.TaskProvider) (taskProvider)).get()
        }
    }

    @CompileDynamic
    static private boolean isIterableProperty(Object o) {
        isListProperty(o) || isSetProperty(o)
    }

    @CompileDynamic
    static private boolean isListProperty(Object o) {
        if (PRE_4_3) {
            false
        } else {
            o instanceof org.gradle.api.provider.ListProperty
        }
    }

    @CompileDynamic
    static private boolean isSetProperty(Object o) {
        if (PRE_4_5) {
            false
        } else {
            o instanceof org.gradle.api.provider.SetProperty
        }
    }

    @CompileDynamic
    @Deprecated
    static private void resolveIterablePropertyTo(List<Task> tasks, Project project, Object o) {
        resolveIterablePropertyTo(tasks, project.tasks, o)
    }

    @CompileDynamic
    static private void resolveIterablePropertyTo(List<Task> tasks, TaskContainer projectTasks, Object o) {
        tasks.addAll(taskize(projectTasks, o.get()))
    }

    @Deprecated
    static private void resolveSingleItemOrIterableTo(List<Task> tasks, Project project, Object o) {
        resolveSingleItemOrIterableTo(tasks, project.tasks, o)
    }

    static private void resolveSingleItemOrIterableTo(List<Task> tasks, TaskContainer projectTasks, Object o) {
        if (o instanceof Iterable) {
            tasks.addAll(taskize(projectTasks, o))
        } else {
            tasks.addAll(taskize(projectTasks, [o]))
        }
    }
}
