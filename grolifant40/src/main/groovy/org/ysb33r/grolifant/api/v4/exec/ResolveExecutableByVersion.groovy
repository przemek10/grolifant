/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.exec

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.v4.AbstractDistributionInstaller
import org.ysb33r.grolifant.api.v4.StringUtils
import org.ysb33r.grolifant.api.v4.exec.DownloaderFactory as DownloaderFactoryV4
import org.ysb33r.grolifant.api.v4.exec.DownloadedExecutable as DownloadedExecutableV4

/** Uses an implementation of an {@link AbstractDistributionInstaller} to find an exe by version number.
 *
 * @since 0.17.0
 */
@CompileStatic
class ResolveExecutableByVersion<T extends AbstractDistributionInstaller> implements NamedResolvedExecutableFactory {

    /**
     *
     * @param <T> An implementation of an {@link AbstractDistributionInstaller}
     *
     * @deprecated Use {@link org.ysb33r.grolifant.api.v4.exec.DownloaderFactory} instead
     */
    @Deprecated
    static interface DownloaderFactory<T> extends DownloaderFactoryV4<T> {
        /** Creates a downloader
         *
         * @param options An arbitrary map of options. This could interpreted by the factory or simply ignored.
         * @param version The version of the exe that is required.
         * @param project The associated Gradle project
         * @return An instance of a downloader
         */
        T create(Map<String, Object> options, String version, Project project)
    }

    /**
     *
     * @param <T>  An implementation of an {@link AbstractDistributionInstaller}
     *
     * @deprecated
     */
    @Deprecated
    static interface DownloadedExecutable<T> extends DownloadedExecutableV4<T> {
    }

    /** Returns {@code name}.
     */
    final String name = NAME

    /** Resolve by downloadable version.
     *
     * @param project Associated project.
     * @param factory Factory for creating downloaders.
     * @param resolver Resolves execution path from download item.
     *
     * @deprecated
     */
    @Deprecated
    ResolveExecutableByVersion(
        Project project,
        final DownloaderFactoryV4 factory,
        final DownloadedExecutableV4 resolver
    ) {
        this.projectOperations = ProjectOperations.create(project)
        this.factory = factory
        this.resolver = resolver
    }

    /** Resolve by downloadable version.
     *
     * @param projectOperations Associated project.
     * @param factory Factory for creating downloaders.
     * @param resolver Resolves execution path from download item.
     *
     * @since 1.0
     */
    ResolveExecutableByVersion(
        ProjectOperations projectOperations,
        final DownloaderFactoryV4 factory,
        final DownloadedExecutableV4 resolver
    ) {
        this.projectOperations = projectOperations
        this.factory = factory
        this.resolver = resolver
    }

    /** Creates {@link org.ysb33r.grolifant.api.v4.exec.ResolvableExecutable} from a tool version.
     *
     * @param options Passed through to the downloader, ignored otherwise.
     * @param from Anything convertible to a string that contains a valid version for the
     *   specific exe / tool / distribution.
     * @return The resolved exe.
     */
    @Override
    ResolvableExecutable build(Map<String, Object> options, Object from) {
        T dnl = (T) (factory.create(options, StringUtils.stringize(from), projectOperations))
        DownloadedExecutableV4 scopedResolver = this.resolver
        new ResolvableExecutable() {
            @Override
            File getExecutable() {
                scopedResolver.getPath(dnl)
            }
        }
    }

    private final ProjectOperations projectOperations
    private final DownloaderFactoryV4 factory
    private final DownloadedExecutableV4 resolver
    private static final String NAME = 'version'
}
