/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.exec;

import org.ysb33r.grolifant.api.v4.AbstractDistributionInstaller;

import java.io.File;

/**
 * Resolves the executable within a downloaded package.
 *
 *  @param <T> An implementation of {@link AbstractDistributionInstaller}
 * @since 1.0
 */
public interface DownloadedExecutable<T extends AbstractDistributionInstaller> {
    /** Given a downloader resolve the path to the exe.
     *
     * @param downloader Downloader as potentially supplied via a
     *   {@link DownloaderFactory}.
     * @return Path on filesystem to exe.
     */
    File getPath(T downloader);
}
