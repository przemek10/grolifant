/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.internal.core

import groovy.transform.CompileStatic
import groovy.transform.Synchronized
import org.gradle.api.Project
import org.ysb33r.grolifant.api.core.LegacyLevel
import org.ysb33r.grolifant.api.core.ProjectOperations

@CompileStatic
class ProjectOperationsLoader {
    @Synchronized
    static ProjectOperations load(Project project) {
        Class loadable
        if (!LegacyLevel.PRE_6_0) {
            try {
                loadable = ProjectOperationsLoader.classLoader.loadClass(V6)
            } catch (ClassNotFoundException e) {
//                if (!LegacyLevel.PRE_6_6) {
//                    project.logger.warn(ConfigurationCacheWarning.missingGrolifant60())
//                }
            }
        }

        if (!loadable && !LegacyLevel.PRE_5_0) {
            try {
                loadable = ProjectOperationsLoader.classLoader.loadClass(V5)
            } catch (ClassNotFoundException e) {
                if (!LegacyLevel.PRE_6_6) {
                    project.logger.warn(ConfigurationCacheWarning.missingGrolifant50())
                }
            }
        }

        if (loadable == null) {
            try {
                loadable = ProjectOperationsLoader.classLoader.loadClass(V4)
            } catch (ClassNotFoundException e) {
                project.logger.warn(
                    'There is no known implementation of org.ysb33r.grolifant.api.ProjectOperations on the classpath'
                )
            }
        }

        loadable ? (ProjectOperations) loadable.newInstance(project) : null
    }

    private static final String V6 = 'org.ysb33r.grolifant.loadable.v6.DefaultProjectOperations'
    private static final String V5 = 'org.ysb33r.grolifant.loadable.v5.DefaultProjectOperations'
    private static final String V4 = 'org.ysb33r.grolifant.loadable.v4.DefaultProjectOperations'
}
