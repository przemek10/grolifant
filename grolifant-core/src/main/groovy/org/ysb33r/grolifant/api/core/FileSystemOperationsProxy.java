/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.core;

import org.gradle.api.Action;
import org.gradle.api.file.CopySpec;
import org.gradle.api.file.DeleteSpec;
import org.gradle.api.tasks.WorkResult;

/**
 * Acts as a proxy for the {@code FileSystemOperations} that was implemented in Gradle 6.0,
 * but allows for a substitute operation on old Gradle versions.
 *
 * @since 1.0.0
 */
public interface FileSystemOperationsProxy {
    /**
     * Copies the specified files.
     * @param action Configures a {@link CopySpec}
     * @return Result of copy to check whether it was successful.
     */
    WorkResult copy(Action<? super CopySpec> action);

    /**
     * Deletes the specified files.
     * @param action Configures a {@link DeleteSpec}
     * @return  Result of deletion to check whether it was successful.
     */
    WorkResult delete(Action<? super DeleteSpec> action);

}
