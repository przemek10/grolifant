/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.loadable.core

import org.gradle.StartParameter
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.file.CopySpec
import org.gradle.api.file.DeleteSpec
import org.gradle.api.file.FileTree
import org.gradle.api.invocation.Gradle
import org.gradle.api.logging.LogLevel
import org.gradle.api.logging.configuration.ConsoleOutput
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.resources.ReadableResource
import org.gradle.api.resources.ResourceHandler
import org.gradle.api.tasks.WorkResult
import org.gradle.process.ExecResult
import org.gradle.process.ExecSpec
import org.gradle.process.JavaExecSpec
import org.ysb33r.grolifant.api.core.ArchiveOperationsProxy
import org.ysb33r.grolifant.api.core.ExecOperationsProxy
import org.ysb33r.grolifant.api.core.FileSystemOperationsProxy
import org.ysb33r.grolifant.api.core.ProjectOperations

abstract class ProjectOperationsProxy implements ProjectOperations {
    /**
     * Creates resource that points to a bzip2 compressed file at the given path.
     *
     * @param compressedFile File evaluated as per {@link #file}.
     * @return Readable resource
     */
    @Override
    ReadableResource bzip2Resource(Object compressedFile) {
        this.resourceHandler.bzip2(file(compressedFile))
    }

    /**
     * Creates resource that points to a gzip compressed file at the given path.
     * @param compressedFile File evaluated as per {@link #file}.
     * @return Readable resource
     */
    @Override
    ReadableResource gzipResource(Object compressedFile) {
        this.resourceHandler.gzip(file(compressedFile))
    }

    /**
     * Performs a copy according to copy specification.
     *
     * @param action Copy specification
     * @return Result of copy operation.
     */
    @Override
    WorkResult copy(Action<? super CopySpec> action) {
        fileSystemOperations.copy(action)
    }

    /**
     * Creates a {@link CopySpec} which can later be used to copy files or create an archive. The given action is used
     * to configure the {@link CopySpec} before it is returned by this method.
     *
     * @param action Action to configure the CopySpec
     * @return The CopySpec
     */
    @Override
    CopySpec copySpec(Action<? super CopySpec> action) {
        CopySpec cs = copySpec()
        action.execute(cs)
        cs
    }

    @Override
    boolean delete(Object... paths) {
        boolean result = true
        for (File it in fileize(paths as List)) {
            if (it.exists()) {
                boolean tmp = it.directory ? it.deleteDir() : it.delete()
                if (!tmp) {
                    result = false
                }
            }
        }
        result
    }

    @Override
    WorkResult delete(Action<? super DeleteSpec> action) {
        fileSystemOperations.delete(action)
    }

    /**
     * Executes the specified external process.
     *
     * @param action
     * @return {@link ExecResult} that can be used to check if the execution worked.
     *
     */
    @Override
    ExecResult exec(Action<? super ExecSpec> action) {
        execOperations.exec(action)
    }

    /**
     * Extensions container for the project.
     *
     * @return Reference to the extensions container
     */
    @Override
    @Deprecated
    ExtensionContainer getExtensions() {
        this.extensions
    }

    /**
     * Gradle user home directory. Usually {@code ~/.gradle} on non -Windows.
     *
     * @return Directory.
     */
    @Override
    Provider<File> getGradleUserHomeDir() {
        this.gradleUserHomeDir
    }

    /**
     * The current project's directory
     *
     * @return Project directory.
     */
    @Override
    File getProjectDir() {
        this.projectDir
    }

    /**
     * A reference to the provider factory.
     *
     * @return {@link ProviderFactory}
     */
    @Override
    ProviderFactory getProviders() {
        this.providerFactory
    }

    /**
     * Whether Gradle is operating in offline mode.
     *
     * @return {@code true} if offline.
     */
    @Override
    boolean isOffline() {
        this.offline
    }

    /**
     * Whether dependencies should be refreshed.
     *
     * @return {@code true} to check dependencies again.
     */
    @Override
    boolean isRefreshDependencies() {
        this.refreshDependencies
    }

    /**
     * Whether tasks should be re-ruin
     *
     * @return {@code true} if tasks were set to be re-run.
     */
    @Override
    boolean isRerunTasks() {
        this.rerunTasks
    }

    /**
     * Executes the specified external java process.
     * @param action
     * @return {@link ExecResult} that can be used to check if the execution worked.
     *
     */
    @Override
    ExecResult javaexec(Action<? super JavaExecSpec> action) {
        execOperations.javaexec(action)
    }

    /**
     * Get the minimum log level for Gradle.
     *
     * @return Log level
     */
    @Override
    LogLevel getGradleLogLevel() {
        this.logLevel
    }

    /**
     * Console output mode
     *
     * @return How the console output has been requested.
     */
    @Override
    ConsoleOutput getConsoleOutput() {
        this.consoleOutput
    }

    /**
     * Returns a TAR tree presentation
     *
     * @param tarPath Path to tar file
     * @return File tree
     */
    @Override
    FileTree tarTree(Object tarPath) {
        archiveOperations.tarTree(tarPath)
    }

    /**
     * Returns a ZIP tree presentation
     *
     * @param zipPath Path to zip file
     * @return File tree
     */
    @Override
    FileTree zipTree(Object zipPath) {
        archiveOperations.zipTree(zipPath)
    }

    abstract protected FileSystemOperationsProxy getFileSystemOperations()

    abstract protected ArchiveOperationsProxy getArchiveOperations()

    abstract protected ExecOperationsProxy getExecOperations()

    protected ProjectOperationsProxy(Project tempProjectReference) {
        this.extensions = tempProjectReference.extensions
        this.providerFactory = tempProjectReference.providers
        this.resourceHandler = tempProjectReference.resources

        Gradle gradle = tempProjectReference.gradle
        StartParameter startParameter = gradle.startParameter

        this.offline = startParameter.offline
        this.logLevel = startParameter.logLevel
        this.refreshDependencies = startParameter.refreshDependencies
        this.rerunTasks = startParameter.rerunTasks
        this.gradleUserHomeDir = providerFactory.provider({ File f -> f }.curry(gradle.gradleUserHomeDir))
        this.consoleOutput = startParameter.consoleOutput
        this.projectDir = tempProjectReference.projectDir
    }

    private final ExtensionContainer extensions // TODO: Remove
    private final ProviderFactory providerFactory
    private final Provider<File> gradleUserHomeDir
    private final File projectDir
    private final boolean offline
    private final boolean refreshDependencies
    private final boolean rerunTasks
    private final LogLevel logLevel
    private final ConsoleOutput consoleOutput
    private final ResourceHandler resourceHandler
}
