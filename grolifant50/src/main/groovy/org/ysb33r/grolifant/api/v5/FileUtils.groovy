/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v5

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.file.CopySpec
import org.gradle.api.file.FileCollection
import org.gradle.api.file.ProjectLayout
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.provider.SetProperty
import org.ysb33r.grolifant.api.core.ClassLocation
import org.ysb33r.grolifant.api.v4.FileUtils as V4FileUtils
import org.ysb33r.grolifant.internal.v4.copyspec.Resolver

import java.nio.file.Path
import java.util.concurrent.Callable

/** Various file utilities that requires at least Gradle 5.0
 *
 * @since 1.0.0
 */
@CompileStatic
class FileUtils {

    /** Converts a string into a string that is safe to use as a file name. T
     *
     * The result will only include ascii characters and numbers, and the "-","_", #, $ and "." characters.
     *
     * @param A potential file name
     * @return A name that is safe on the local filesystem of the current operating system.
     */
    @CompileDynamic
    static String toSafeFileName(String name) {
        V4FileUtils.toSafeFileName(name)
    }

    /** Converts a collection of String into a {@link Path} with all parts guarantee to be safe file parts
     *
     * @param parts File path parts
     * @return File path
     */
    static Path toSafePath(String... parts) {
        V4FileUtils.toSafePath(parts)
    }

    /** Converts a collection of String into a {@@link File} with all parts guarantee to be safe file parts
     *
     * @param parts File path parts
     * @return File path
     */
    static File toSafeFile(String... parts) {
        toSafePath(parts).toFile()
    }

    /** Returns the file collection that a {@link CopySpec} describes.
     *
     * @param copySpec An instance of a {@link CopySpec}
     * @return Result collection of files.
     */
    static FileCollection filesFromCopySpec(CopySpec copySpec) {
        Resolver.resolveFiles(copySpec)
    }

    /** Provides a list of directories below another directory
     *
     * @param distDir Directory
     * @return List of directories. Can be empty if, but never {@code null}
     *   supplied directory.
     */
    static List<File> listDirs(File distDir) {
        V4FileUtils.listDirs(distDir)
    }

    /** Returns the classpath location for a specific class
     *
     * @param aClass Class to find.
     * @return Location of class. Can be {@code null} which means class has been found, but cannot be placed
     *   on classpath
     * @throw ClassNotFoundException*
     */
    @SuppressWarnings('DuplicateStringLiteral')
    static ClassLocation resolveClassLocation(Class aClass) {
        V4FileUtils.resolveClassLocation(aClass)
    }

    /** Converts a file-like object to a {@link java.io.File} instance with project context.
     *
     * Converts any of the following recursively until it gets to a file:
     *
     * <ul>
     *   <li> {@code CharSequence} including {@code String} and {@code GString}.
     *   <li> {@link java.io.File}.
     *   <li> {@link java.nio.file.Path} is it is associated with the default provider
     *   <li> URLs and URis of {@code file:} schemes.
     *   <li> Groovy Closures.
     *   <li> {@link java.util.concurrent.Callable}.
     *   <li> {@link org.gradle.api.provider.Provider}.
     *   <li> {@link org.gradle.api.file.Directory}
     *   <li> {@link org.gradle.api.resources.TextResource}
     *   <li> {@link java.util.Optional} of any of the above
     * </ul>
     *
     * @param projectLayout Project layout
     * @param file
     * @return File instance.
     */
    static File fileize(ProjectLayout projectLayout, Object file) {
        switch (file) {
            case Optional:
                fileize(projectLayout, ((Optional) file).get())
                break
            default:
                projectLayout.files(file).singleFile
        }
    }

    /** Converts a collection of file-like objects to a a list of  {@link java.io.File} instances within
     * a project layout.
     *
     * It will convert anything that the singular version of {@link #fileize(ProjectLayout projectLayout, Object o)}
     * can do.
     * In addition it will recursively resolve any collections that result out of resolving the supplied items.
     *
     * @param project projectLayout Project layout
     * @param files List of object to evaluate
     * @return List of resolved files.
     */
    static List<File> fileize(ProjectLayout projectLayout, Iterable<Object> files) {
        List<File> collection = []

        for (Object item in files) {
            if (isIterableProperty(item)) {
                resolveIterablePropertyTo(projectLayout, collection, item)
            } else {
                switch (item) {
                    case Map:
                        collection.addAll(fileize(projectLayout, (Iterable) ((Map) item).values()))
                        break
                    case Iterable:
                        collection.addAll(fileize(projectLayout, (Iterable) item))
                        break
                    case Provider:
                        resolveSingleItemOrIterableTo(projectLayout, collection, ((Provider) item).get())
                        break
                    case Callable:
                        resolveSingleItemOrIterableTo(projectLayout, collection, ((Callable) item).call())
                        break
                    default:
                        collection.add(fileize(projectLayout, item))
                }
            }
        }
        collection
    }

    /**
     * Returns the path of one File relative to another.
     *
     * @param target the target directory
     * @param base the base directory
     * @return target's path relative to the base directory
     * @throws IOException if an error occurs while resolving the files' canonical names.
     *
     * @since 1.0.0
     */
    static String relativize(File base, File target) {
        base.toPath().relativize(target.toPath()).toFile().toString()
    }

    /**
     * Returns the path of one Path relative to another.
     *
     * @param target the target directory
     * @param base the base directory
     * @return target's path relative to the base directory
     * @throws IOException if an error occurs while resolving the files' canonical names.
     *
     * @since 1.0.0
     */
    static String relativize(Path base, Path target) {
        base.relativize(target).toFile().toString()
    }

    /** Updates a Provider.
     *
     * Update property
     * otherwise the provider will be assigned a new Provider instance.
     *
     * @param projectLayout Project layout
     * @param providers Provider factory
     * @param provider Current property
     * @param stringy Value that should be lazy-resolved.
     */
    static void updateFileProperty(
        ProjectLayout projectLayout,
        ProviderFactory providers,
        Property<File> provider,
        Object file
    ) {
        Provider<File> newProvider = providers.provider({ ->
            fileize(projectLayout, file)
        } as Callable<File>)
        provider.set(newProvider)
    }

    static private boolean isIterableProperty(Object o) {
        o instanceof ListProperty || o instanceof SetProperty
    }

    @CompileDynamic
    static private void resolveIterablePropertyTo(ProjectLayout projectLayout, List<File> files, Object o) {
        files.addAll(fileize(projectLayout, o.get()))
    }

    static private void resolveSingleItemOrIterableTo(ProjectLayout projectLayout, List<File> files, Object o) {
        if (o instanceof Iterable) {
            files.addAll(fileize(projectLayout, o))
        } else {
            files.addAll(fileize(projectLayout, [o]))
        }
    }
}

